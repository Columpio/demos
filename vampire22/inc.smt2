(set-logic HORN)
(declare-datatype Nat ((Z) (S (prev Nat))))

(declare-fun inc (Nat Nat) Bool)
(assert (inc Z (S Z)))
(assert (forall ((x Nat) (r Nat)) (=> (inc x r) (inc (S x) (S r)))))

(assert (forall ((x Nat) (r Nat)) (=> (and (inc x r) (not (= r (S x)))) false)))

(check-sat)
(get-model)

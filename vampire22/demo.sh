### meta
# z3                is taken from https://github.com/Z3Prover/z3/
# eld (Eldarica)    is taken from https://github.com/uuverifiers/eldarica
# ringen (RInGen)   is taken from https://github.com/Columpio/RInGen

### before start: terminal at left, split; preso at right
PS1="$ "

### preso
## demo 1
vim demo.hs
./preprocess drop_id
vim drop_id.Transformed.0.smt2
vim drop_id.clean.smt2
z3 drop_id.clean.smt2
vim drop_zero.smt2
z3 drop_zero.smt2

## demo 2
# uncomment (get-proof) in drop_id.clean.smt2
z3 drop_id.clean.smt2
eld -cex drop_id.clean.smt2
vim drop_id.cex

## demo 3
# uncomment (get-model) in drop_zero.smt2
vim drop_zero.smt2
z3 drop_zero.smt2

## demo 4
vim inc.smt2
z3 inc.smt2
vim even.smt2
z3 even.smt2

## demo 5
ringen solve cvc4f -q even.smt2
z3 evenLeft.smt2
eld evenLeft.smt2
ringen solve cvc4f -q evenLeft.smt2
z3 stlc.smt2
eld stlc.smt2
ringen solve cvc4f -e -q stlc.smt2
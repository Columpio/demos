module Demo where
import Prelude (Eq)
import Tip

data Nat = S Nat | Z deriving (Eq)

drop :: Nat -> [Nat] -> [Nat]
drop Z     xs     = xs
drop _     []     = []
drop (S n) (_:xs) = drop n xs

drop_id n xs = drop n (drop n xs) === drop n xs -- unsafe
drop_zero xs = drop Z xs === xs                 -- safe